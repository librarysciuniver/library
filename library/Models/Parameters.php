<?php

namespace Library\Models;

/**
 * Класс
 *
 * Class Parameters
 */
class Parameters
{
    /**
     * Номинальная мощность
     *
     * @var float
     */
    protected float $ratedPower;

    /**
     * Частота вращения коленчатого вала при  номинальной ммощности
     *
     * @var float
     */
    protected float $crankshaftSpeedAtRatedPower;

    /**
     * Применяемое топливо
     *
     * @var string
     */
    protected string $usedFuel;

    /**
     * Низшая теплотворная способность топлива
     *
     * @var float
     */
    protected float $netCalorificValueOfFuel;

    /**
     * Молекулярная масса топлива
     *
     * @var float
     */
    protected float $molecularWeightOfFuel;

    /**
     * Степень сжатия
     *
     * @var float
     */
    protected float $compressionRatio;

    /**
     * Состав топлива C
     *
     * @var float
     */
    protected float $fuelCompositionC;

    /**
     * Состав топлива H
     *
     * @var float
     */
    protected float $fuelCompositionH;

    /**
     * Состав топлива От
     *
     * @var float
     */
    protected float $fuelCompositionOt;

    /**
     * Коэффициент избытка топлива на номинальном режиме работы двигателя
     *
     * @var float
     */
    protected float $excessFuelRatioAtNominalEngineOperation;

    /**
     * Давление окружающей среды
     *
     * @var float
     */
    protected float $ambientPressure;

    /**
     * Удельная газовая постоянная воздуха
     *
     * @var float
     */
    protected float $specificGasConstantOfAir;

    /**
     * Температура окружающей среды
     *
     * @var float
     */
    protected float $ambientTemperature;

    /**
     * Средняя скорость поршня
     *
     * @var float
     */
    protected float $averagePistonSpeed;

    /**
     * Отношение площади поршня к проходному сечению
     *
     * @var float
     */
    protected float $ratioOfPistonAreaToFlowArea;

    /**
     * Сопротивление впускной системы
     *
     * @var float
     */
    protected float $intakeSystemResistance;

    /**
     * Степень подогрева свежего заряда
     *
     * @var float
     */
    protected float $freshChargeHeatingDegree;

    /**
     * Температура остаточных газов
     *
     * @var float
     */
    protected float $residualGasTemperature;

    /**
     * Показатель политропы сжатия
     *
     * @var float
     */
    protected float $сompressionPolytropicExponent;

    /**
     * Средня мольная теплоемкость в конце сжатия для остаточных газов
     *
     * @var float
     */
    protected float $averageMolarHeatCapacityAtTheEndOfCompressionForResidualGasese;

    /**
     * Показатель политропы расширения
     *
     * @var float
     */
    protected float $expansionPolytropicExponent;

    /**
     * Parameters constructor.
     *
     * @param float $ratedPower Номинальная мощность
     * @param float $crankshaftSpeedAtRatedPower Частота вращения коленчатого вала при  номинальной ммощности
     * @param string $usedFuel Применяемое топливо
     * @param float $netCalorificValueOfFuel Низшая теплотворная способность топлива
     * @param float $molecularWeightOfFuel Молекулярная масса топлива
     * @param float $compressionRatio Степень сжатия
     * @param float $fuelCompositionC Состав топлива C
     * @param float $fuelCompositionH Состав топлива H
     * @param float $fuelCompositionOt Состав топлива От
     * @param float $excessFuelRatioAtNominalEngineOperation Коэффициент избытка топлива на номинальном режиме работы двигателя
     * @param float $ambientPressure Давление окружающей среды
     * @param float $specificGasConstantOfAir Удельная газовая постоянная воздуха
     * @param float $ambientTemperature Температура окружающей среды
     * @param float $averagePistonSpeed Средняя скорость поршня
     * @param float $ratioOfPistonAreaToFlowArea Отношение площади поршня к проходному сечению
     * @param float $intakeSystemResistance Сопротивление впускной системы
     * @param float $freshChargeHeatingDegree Степень подогрева свежего заряда
     * @param float $residualGasTemperature Температура остаточных газов
     * @param float $сompressionPolytropicExponent Показатель политропы сжатия
     * @param float $averageMolarHeatCapacityAtTheEndOfCompressionForResidualGasese Средня мольная теплоемкость в конце сжатия для остаточных газов
     * @param float $expansionPolytropicExponent Показатель политропы расширения
     */
    public function __construct(
        float $ratedPower,
        float $crankshaftSpeedAtRatedPower,
        string $usedFuel,
        float $netCalorificValueOfFuel,
        float $molecularWeightOfFuel,
        float $compressionRatio,
        float $fuelCompositionC,
        float $fuelCompositionH,
        float $fuelCompositionOt,
        float $excessFuelRatioAtNominalEngineOperation,
        float $ambientPressure,
        float $specificGasConstantOfAir,
        float $ambientTemperature,
        float $averagePistonSpeed,
        float $ratioOfPistonAreaToFlowArea,
        float $intakeSystemResistance,
        float $freshChargeHeatingDegree,
        float $residualGasTemperature,
        float $сompressionPolytropicExponent,
        float $averageMolarHeatCapacityAtTheEndOfCompressionForResidualGasese,
        float $expansionPolytropicExponent
    ) {
        $this->ratedPower = $ratedPower;
        $this->crankshaftSpeedAtRatedPower = $crankshaftSpeedAtRatedPower;
        $this->usedFuel = $usedFuel;
        $this->netCalorificValueOfFuel = $netCalorificValueOfFuel;
        $this->molecularWeightOfFuel = $molecularWeightOfFuel;
        $this->compressionRatio = $compressionRatio;
        $this->fuelCompositionC = $fuelCompositionC;
        $this->fuelCompositionH = $fuelCompositionH;
        $this->fuelCompositionOt = $fuelCompositionOt;
        $this->excessFuelRatioAtNominalEngineOperation = $excessFuelRatioAtNominalEngineOperation;
        $this->ambientPressure = $ambientPressure;
        $this->specificGasConstantOfAir = $specificGasConstantOfAir;
        $this->ambientTemperature = $ambientTemperature;
        $this->averagePistonSpeed = $averagePistonSpeed;
        $this->ratioOfPistonAreaToFlowArea = $ratioOfPistonAreaToFlowArea;
        $this->intakeSystemResistance = $intakeSystemResistance;
        $this->freshChargeHeatingDegree = $freshChargeHeatingDegree;
        $this->residualGasTemperature = $residualGasTemperature;
        $this->сompressionPolytropicExponent = $сompressionPolytropicExponent;
        $this->averageMolarHeatCapacityAtTheEndOfCompressionForResidualGasese = $averageMolarHeatCapacityAtTheEndOfCompressionForResidualGasese;
        $this->expansionPolytropicExponent = $expansionPolytropicExponent;
    }

    /**
     * @return float
     */
    public function getRatedPower(): float
    {
        return $this->ratedPower;
    }

    /**
     * @return float
     */
    public function getCrankshaftSpeedAtRatedPower(): float
    {
        return $this->crankshaftSpeedAtRatedPower;
    }

    /**
     * @return string
     */
    public function getUsedFuel(): string
    {
        return $this->usedFuel;
    }

    /**
     * @return float
     */
    public function getNetCalorificValueOfFuel(): float
    {
        return $this->netCalorificValueOfFuel;
    }

    /**
     * @return float
     */
    public function getMolecularWeightOfFuel(): float
    {
        return $this->molecularWeightOfFuel;
    }

    /**
     * @return float
     */
    public function getCompressionRatio(): float
    {
        return $this->compressionRatio;
    }

    /**
     * @return float
     */
    public function getFuelCompositionC(): float
    {
        return $this->fuelCompositionC;
    }

    /**
     * @return float
     */
    public function getFuelCompositionH(): float
    {
        return $this->fuelCompositionH;
    }

    /**
     * @return float
     */
    public function getFuelCompositionOt(): float
    {
        return $this->fuelCompositionOt;
    }

    /**
     * @return float
     */
    public function getExcessFuelRatioAtNominalEngineOperation(): float
    {
        return $this->excessFuelRatioAtNominalEngineOperation;
    }

    /**
     * @return float
     */
    public function getAmbientPressure(): float
    {
        return $this->ambientPressure;
    }

    /**
     * @return float
     */
    public function getSpecificGasConstantOfAir(): float
    {
        return $this->specificGasConstantOfAir;
    }

    /**
     * @return float
     */
    public function getAmbientTemperature(): float
    {
        return $this->ambientTemperature;
    }

    /**
     * @return float
     */
    public function getAveragePistonSpeed(): float
    {
        return $this->averagePistonSpeed;
    }

    /**
     * @return float
     */
    public function getRatioOfPistonAreaToFlowArea(): float
    {
        return $this->ratioOfPistonAreaToFlowArea;
    }

    /**
     * @return float
     */
    public function getIntakeSystemResistance(): float
    {
        return $this->intakeSystemResistance;
    }

    /**
     * @return float
     */
    public function getFreshChargeHeatingDegree(): float
    {
        return $this->freshChargeHeatingDegree;
    }

    /**
     * @return float
     */
    public function getResidualGasTemperature(): float
    {
        return $this->residualGasTemperature;
    }

    /**
     * @return float
     */
    public function getCompressionPolytropicExponent(): float
    {
        return $this->сompressionPolytropicExponent;
    }

    /**
     * @return float
     */
    public function getAverageMolarHeatCapacityAtTheEndOfCompressionForResidualGasese(): float
    {
        return $this->averageMolarHeatCapacityAtTheEndOfCompressionForResidualGasese;
    }

    /**
     * @return float
     */
    public function getExpansionPolytropicExponent(): float
    {
        return $this->expansionPolytropicExponent;
    }
}
