<?php

namespace Library\Utils;

use Library\Models\Parameters;

/**
 * Утилита для определения параметров процесса расширения и выпуска
 *
 * Class DeterminationOfTheExpansionAndReleaseProcessUtil
 */
class DeterminationOfTheExpansionAndReleaseProcessUtil
{
  /**
   * Получить давление в конце процесса расширения
   * 
   * Pb
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getPressureAtTheEndOfTheExpansionProcess(Parameters $parameters): float
  {
    return DeterminationOfTheCombustionProcessUtil::getPressureAtTheEndOfTheCombustionProcess($parameters) /
      $parameters->getCompressionRatio() ** $parameters->getExpansionPolytropicExponent($parameters);
  }

  /**
   * Получить температуру в конце процесса расширения
   * 
   * Tb
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getTemperatureAtTheEndOfTheExpansionProcess(Parameters $parameters): float
  {
    return DeterminationOfTheCombustionProcessUtil::getTemperatureAtTheEndOfTheCombustionProcess($parameters) /
      $parameters->getCompressionRatio() ** ($parameters->getExpansionPolytropicExponent($parameters) - 1);
  }

  /**
   * Получить температуру остаточных газов
   * 
   * Tк
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getPresidualGasTemperature(Parameters $parameters): float
  {
    return static::getTemperatureAtTheEndOfTheExpansionProcess($parameters) /
      ((static::getPressureAtTheEndOfTheExpansionProcess($parameters) / DeterminationOfTheIntakeProcessUtil::getResidualGasPressureAtRatedPower($parameters)) ** (1 / 3));
  }
}
