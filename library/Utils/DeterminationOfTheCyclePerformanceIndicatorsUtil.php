<?php

namespace Library\Utils;

use Library\Models\Parameters;

/**
 * Утилита для определения параметров работы цикла 
 *
 * Class DeterminationOfTheCyclePerformanceIndicatorsUtil
 */
class DeterminationOfTheCyclePerformanceIndicatorsUtil
{
  /**
   * Получить теоритическое среднее индикаторное давление
   * 
   * P`i
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getTheoreticalAverageIndicatorPressure(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить действительное среднее индикаторное давление
   * 
   * Pi
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getActualAverageIndicatedPressure(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить индикаторный КПД
   * 
   * ni
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getIndicatorEfficiency(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить индикаторный удельный расход топлива
   * 
   * gi
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getIndicatorSpecificFuelConsumption(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить давление механических потерь
   * 
   * Pm
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getMechanicalLossPressure(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить среднюю скорость поршня
   * 
   * Vp.avg
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getAveragePistonSpeed(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить эффективное давление
   * 
   * Pe
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getEffectivePressure(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить механический КПД
   * 
   * nm
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getMechanicalEfficiency(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить эффективный КПД
   * 
   * ne
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getEffectiveEfficiency(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить эффективный удельный расход топлива
   * 
   * ge
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getEffectiveSpecificFuelConsumption(Parameters $parameters): float
  {
    return 0;
  }
}
