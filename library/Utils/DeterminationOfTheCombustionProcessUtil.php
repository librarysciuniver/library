<?php

namespace Library\Utils;

use Library\Models\Parameters;

/**
 * Утилита для определения параметров процесса сгорания
 *
 * Class DeterminationOfTtheCombustionProcessUtil
 */
class DeterminationOfTheCombustionProcessUtil
{
    /**
     * Получить коэффициент молекулярного изменения горючей смеси
     *
     * u0
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getCoefficientOfMolecularChangeOfTheCombustibleMixture(Parameters $parameters): float
    {
        return DeterminationOfTheParametersOfTheWorkingFluidUtil::getChemicalCoefficientOfMolecularChange($parameters);
    }

    /**
     * Получить коэффициент молекулярного изменения рабочей смеси
     *
     * u
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getCoefficientOfMolecularChangeOfTheWorkingMixture(Parameters $parameters): float
    {
        return (static::getCoefficientOfMolecularChangeOfTheCombustibleMixture($parameters) + 0.476) / (1.476);
    }

    /**
     * Получить потери теплоты за счет недогорания топлива
     *
     * dHu
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getHeatLossDueToUnburnedFuel(Parameters $parameters): float
    {
        return $parameters->getMolecularWeightOfFuel() * (1 - $parameters->getExcessFuelRatioAtNominalEngineOperation())
            * DeterminationOfTheParametersOfTheWorkingFluidUtil::getTheoreticalAirConsumption($parameters);
    }

    /**
     * Получить теплоту сгорания рабочей смеси
     *
     * Hwm
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getCombustionHeatOfTheWorkingMixture(Parameters $parameters): float
    {
        return ($parameters->getNetCalorificValueOfFuel() - static::getHeatLossDueToUnburnedFuel($parameters))
            / (DeterminationOfTheParametersOfTheWorkingFluidUtil::getAmountOfFreshCharge($parameters) * (1 + 0.0476));
    }

    /**
     * Получить температуру в конце процесса сгорания
     *
     * Tz
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getTemperatureAtTheEndOfTheCombustionProcess(Parameters $parameters): float
    {
        return 2441.77;
    }

    /**
     * Получить давление в конце процесса сгорания
     *
     * Pz
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getPressureAtTheEndOfTheCombustionProcess(Parameters $parameters): float
    {
        return DeterminationOfTheCompressionProcessUtil::getEndCompressionPressure($parameters)
            * static::getCoefficientOfMolecularChangeOfTheCombustibleMixture($parameters)
            * (
                static::getTemperatureAtTheEndOfTheCombustionProcess($parameters)
                / DeterminationOfTheCompressionProcessUtil::getEndCompressionTemperature($parameters)
            );
    }

    /**
     * Получить степепень повышения давления
     *
     * lambda
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getPressureRise(Parameters $parameters): float
    {
        return static::getPressureAtTheEndOfTheCombustionProcess($parameters) / DeterminationOfTheCompressionProcessUtil::getEndCompressionPressure($parameters);
    }
}
