<?php

namespace Library\Utils;

use Library\Models\Parameters;

/**
 * Утилита для определения параметров процесса сжатия
 *
 * Class DeterminationOfTheCompressionProcessUtil
 */
class DeterminationOfTheCompressionProcessUtil
{
  /**
   * Получить давление в конце сжатия
   * 
   * Pc
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getEndCompressionPressure(Parameters $parameters): float
  {
    return DeterminationOfTheIntakeProcessUtil::getPressureInTheEngineCylinderAtTheEndOfTheIntake($parameters) * $parameters->getCompressionRatio() ** $parameters->getCompressionPolytropicExponent();
  }

  /**
   * Получить температуру в конце сжатия
   * 
   * Tc
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getEndCompressionTemperature(Parameters $parameters): float
  {
    return DeterminationOfTheIntakeProcessUtil::getWorkingFluidTemperatureAtTheEndOfFilling($parameters) * $parameters->getCompressionRatio() ** ($parameters->getCompressionPolytropicExponent() - 1);
  }

  /**
   * Получить среднюю мольную теплоемкость в конце сжатия для свежей смеси
   * 
   * mcv
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getAverageMolarHeatCapacityAtTheEndOfCompressionForFreshMixture(Parameters $parameters): float
  {
    return 20.6 + 2.638 * 10 ** -3 * (static::getEndCompressionTemperature($parameters) - 273);
  }

  /**
   * Получить среднюю мольную теплоемкость в конце сжатия для рабочей смеси
   * 
   * mcv`
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getAverageMolarHeatCapacityAtTheEndOfCompressionForTheWorkingMixture(Parameters $parameters): float
  {
    return (1 / (1 + DeterminationOfTheIntakeProcessUtil::getResidualGasRatio($parameters))) * (static::getAverageMolarHeatCapacityAtTheEndOfCompressionForFreshMixture($parameters) + DeterminationOfTheIntakeProcessUtil::getResidualGasRatio($parameters) * 23.825);
  }
}
