<?php

namespace Library\Utils;

use Library\Models\Parameters;

/**
 * Утилита для определения внешней скоростной характеристики
 *
 * Class DeterminationOfTheExternalSpeedCharacteristicUtil
 */
class DeterminationOfTheExternalSpeedCharacteristicUtil
{
  /**
   * Получить эффективную мощность
   * 
   * Nex
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getEffectivePower(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить крутящий момент
   * 
   * Mex
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getTorque(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить удельный эффективный расход топлива
   * 
   * gex
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getSpecificEffectiveFuelConsumption(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить часовой расход топлива
   * 
   * Gtx
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getFuelConsumptionPerHour(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить среднее эффективное давление 
   * 
   * pex
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getAverageEffectivePressure(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить коэффициент наполнения
   * 
   * nv
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getFillingFactor(Parameters $parameters): float
  {
    return 0;
  }
}

