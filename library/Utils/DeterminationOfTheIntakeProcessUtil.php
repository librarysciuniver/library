<?php

namespace Library\Utils;

use Library\Models\Parameters;

/**
 * Утилита для определения параметров процесса впуска
 *
 * Class DeterminationOfTheIntakeProcessUtil
 */
class DeterminationOfTheIntakeProcessUtil
{
  /**
   * Получить плотность воздуха
   *
   * pk
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getAirВensity(Parameters $parameters): float
  {
    return ($parameters->getAmbientPressure() * 10 ** 6) /
      ($parameters->getSpecificGasConstantOfAir() * $parameters->getAmbientTemperature());
  }

  /**
   * Получить ход поршня
   *
   * Sp
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getPistonStroke(Parameters $parameters): float
  {
    return (30 * $parameters->getAveragePistonSpeed()) / $parameters->getCrankshaftSpeedAtRatedPower();
  }

  /**
   * Получить скорость свежего заряда в проходном сечении клапана
   *
   * winj
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getFreshChargeSpeed(Parameters $parameters): float
  {
    return 0.05433 * static::getPistonStroke($parameters) *
      $parameters->getCrankshaftSpeedAtRatedPower() * $parameters->getRatioOfPistonAreaToFlowArea();
  }

  /**
   * Получить давление в цилиндре двигателя в конце впуска
   * 
   * Pa
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getPressureInTheEngineCylinderAtTheEndOfTheIntake(Parameters $parameters): float
  {
    return $parameters->getAmbientPressure() - $parameters->getIntakeSystemResistance() *
      (static::getFreshChargeSpeed($parameters) ** 2 / 2) * static::getAirВensity($parameters) * 10 ** -6;
  }

  /**
   * Получить давление остаточных газов при номинальной мощности
   * 
   * Pr
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getResidualGasPressureAtRatedPower(Parameters $parameters): float
  {
    return $parameters->getAmbientPressure() +  (0.539 * 10 ** -5) * $parameters->getCrankshaftSpeedAtRatedPower();
  }

  /**
   * Получить коэффициент остаточных газов
   * 
   * Yr
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getResidualGasRatio(Parameters $parameters): float
  {
    return (($parameters->getAmbientTemperature() + $parameters->getFreshChargeHeatingDegree()) /
      $parameters->getResidualGasTemperature()) * (static::getResidualGasPressureAtRatedPower($parameters) /
      (static::getPressureInTheEngineCylinderAtTheEndOfTheIntake($parameters) * $parameters->getCompressionRatio() - static::getResidualGasPressureAtRatedPower($parameters)));
  }

  /**
   * Получить температуру рабочего тела в конце наполнения
   * 
   * Ta
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getWorkingFluidTemperatureAtTheEndOfFilling(Parameters $parameters): float
  {
    return ($parameters->getAmbientTemperature() + $parameters->getFreshChargeHeatingDegree() +
      static::getResidualGasRatio($parameters) * $parameters->getResidualGasTemperature()) / (1 + static::getResidualGasRatio($parameters));
  }

  /**
   * Получить коэффициент наполнения
   * 
   * nv
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getFillingFactor(Parameters $parameters): float
  {
    return ($parameters->getAmbientTemperature() / ($parameters->getAmbientTemperature() + $parameters->getFreshChargeHeatingDegree())) * ((static::getPressureInTheEngineCylinderAtTheEndOfTheIntake($parameters) * $parameters->getCompressionRatio() - static::getResidualGasPressureAtRatedPower($parameters)) / ($parameters->getAmbientPressure() * ($parameters->getCompressionRatio() - 1)));
  }
}
