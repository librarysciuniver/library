<?php

namespace Library\Utils;

use Library\Models\Parameters;

/**
 * Утилита для определения параметров рабочего тела
 *
 * Class DeterminationOfTheParametersOfTheWorkingFluidUtil
 */
class DeterminationOfTheParametersOfTheWorkingFluidUtil
{
    /**
     * Получить теоретический расход воздуха
     *
     * L0
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getTheoreticalAirConsumption(Parameters $parameters): float
    {
        return 1 / (12 * 0.21) * ($parameters->getFuelCompositionC() + 3 * ($parameters->getFuelCompositionH() - $parameters->getFuelCompositionOt() / 8));
    }

    /**
     * Получить действительный расход воздуха
     *
     * L
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getActualAirConsumption(Parameters $parameters): float
    {
        $theoreticalAirConsumption = static::getTheoreticalAirConsumption($parameters);

        return $parameters->getExcessFuelRatioAtNominalEngineOperation() * $theoreticalAirConsumption;
    }

    /**
     * Получить количество свежего заряда
     *
     * M1
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getAmountOfFreshCharge(Parameters $parameters): float
    {
        return static::getActualAirConsumption($parameters) + 1 / $parameters->getMolecularWeightOfFuel();
    }

    /**
     * Получить количество выделяемого CO
     *
     * Mco
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getCoAmount(Parameters $parameters): float
    {
        return 0.418 * static::getTheoreticalAirConsumption($parameters) * ((1 - $parameters->getExcessFuelRatioAtNominalEngineOperation()) / (1 + 0.45));
    }

    /**
     * Получить количество выделяемого CO2
     *
     * Mco2
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getCo2Amount(Parameters $parameters): float
    {
        return $parameters->getFuelCompositionC() / 12 - static::getCoAmount($parameters);
    }

    /**
     * Получить количество выделяемого H2
     *
     * Mh2
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getH2Amount(Parameters $parameters): float
    {
        return 0.45 * static::getCoAmount($parameters);
    }

    /**
     * Получить количество выделяемого H2O
     *
     * Mh2o
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getH2oAmount(Parameters $parameters): float
    {
        return $parameters->getFuelCompositionH() / 2 - static::getH2Amount($parameters);
    }

    /**
     * Получить количество выделяемого N2
     *
     * Mn2
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getN2Amount(Parameters $parameters): float
    {
        return 0.791 * $parameters->getExcessFuelRatioAtNominalEngineOperation() * static::getTheoreticalAirConsumption($parameters);
    }

    /**
     * Получить сумарное количество продуктов сгорания
     *
     * M2
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getSumOfCombustionProduct(Parameters $parameters): float
    {
        return static::getCoAmount($parameters)
            + static::getCo2Amount($parameters)
            + static::getH2Amount($parameters)
            + static::getH2oAmount($parameters)
            + static::getN2Amount($parameters);
    }

    /**
     * Получить химический коэффициент молекулярного изменения
     *
     * B'
     *
     * @param Parameters $parameters
     * @return float
     */
    public static function getChemicalCoefficientOfMolecularChange(Parameters $parameters): float
    {
        return static::getSumOfCombustionProduct($parameters) / static::getAmountOfFreshCharge($parameters);
    }
}
