<?php

namespace Library\Utils;

use Library\Models\Parameters;

/**
 * Утилита для определения размеров цилиндра и двигателя
 *
 * Class DeterminationOtTheMainDimensionsOfTheCylinderAndEngineUtil
 */
class DeterminationOtTheMainDimensionsOfTheCylinderAndEngineUtil
{
  /**
   * Получить литраж двигателя
   * 
   * Vl
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getEngineDisplacement(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить рабочий объем одного цилиндра
   * 
   * Vh
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getWorkingVolumeOfOneCylinder(Parameters $parameters): float
  {
    return 0;
  }
  
  /**
   * Получить диаметр цилиндра
   * 
   * D
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getCylinderDiameter(Parameters $parameters): float
  {
    return 0;
  }
  
  /**
   * Получить площадь поршня
   * 
   * Fn
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getPistonArea(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить индикаторную мощность двигателя
   * 
   * Ni
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getEngineIndicatedPower(Parameters $parameters): float
  {
    return 0;
  }

  /**
   * Получить расчетную эффективную мощность двигателя
   * 
   * Ne
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getEffectiveEnginePower(Parameters $parameters): float
  {
    return 0;
  }
  
  /**
   * Получить часовой расход топлива
   * 
   * Gf
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getFuelConsumptionPerHour(Parameters $parameters): float
  {
    return 0;
  }
  
  /**
   * Получить расчетный крутящий момент
   * 
   * Me
   *
   * @param Parameters $parameters
   * @return float
   */
  public static function getDesignTorque(Parameters $parameters): float
  {
    return 0;
  }
}

