<?php

namespace Tests;

use Library\Models\Parameters;
use Library\Utils\DeterminationOfTheParametersOfTheWorkingFluidUtil;
use PHPUnit\Framework\TestCase;

/**
 * Тест определения параметров рабочего тела
 *
 * Class DeterminationOfTheParametersOfTheWorkingFluidServiceTest
 * @package Tests
 */
class DeterminationOfTheParametersOfTheWorkingFluidServiceTest extends TestCase
{
    /** @test */
    public function getTheoreticalAirFlowTest()
    {
        $ratedPower = 56;
        $crankshaftSpeedAtRatedPower = 5600;
        $usedFuel = 'Бензин А-95';
        $netCalorificValueOfFuel = 44;
        $molecularWeightOfFuel = 114;
        $compressionRatio = 8.9;
        $fuelCompositionC = 0.885;
        $fuelCompositionH = 0.145;
        $fuelCompositionOt = 0;
        $excessFuelRatioAtNominalEngineOperation = 0.9;
        $ambientPressure = 0.1;
        $specificGasConstantOfAir = 287;
        $ambientTemperature = 293;
        $averagePistonSpeed = 8.6;
        $ratioOfPistonAreaToFlowArea = 5.0;
        $intakeSystemResistance = 3.6;
        $freshChargeHeatingDegree = 15;
        $residualGasTemperature = 1110;
        $compressionPolytropicExponent = 1.3632;
        $averageMolarHeatCapacityAtTheEndOfCompressionForResidualGasese = 23.825;
        $expansionPolytropicExponent = 1.247;

        $parameters = new Parameters(
            $ratedPower,
            $crankshaftSpeedAtRatedPower,
            $usedFuel,
            $netCalorificValueOfFuel,
            $molecularWeightOfFuel,
            $compressionRatio,
            $fuelCompositionC,
            $fuelCompositionH,
            $fuelCompositionOt,
            $excessFuelRatioAtNominalEngineOperation,
            $ambientPressure,
            $specificGasConstantOfAir,
            $ambientTemperature,
            $averagePistonSpeed,
            $ratioOfPistonAreaToFlowArea,
            $intakeSystemResistance,
            $freshChargeHeatingDegree,
            $residualGasTemperature,
            $compressionPolytropicExponent,
            $averageMolarHeatCapacityAtTheEndOfCompressionForResidualGasese,
            $expansionPolytropicExponent
        );

        self::assertEquals(
            1.081,
            round(DeterminationOfTheParametersOfTheWorkingFluidUtil::getChemicalCoefficientOfMolecularChange($parameters), 3)
        );
    }
}
